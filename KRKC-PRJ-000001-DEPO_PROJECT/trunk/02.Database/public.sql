/*
Navicat PGSQL Data Transfer

Source Server         : LocalEmek
Source Server Version : 90504
Source Host           : localhost:5432
Source Database       : DepoProjectDB
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90504
File Encoding         : 65001

Date: 2017-03-27 11:06:51
*/


-- ----------------------------
-- Sequence structure for tb_user_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_user_seq";
CREATE SEQUENCE "public"."tb_user_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."tb_user_seq"', 1, true);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_user";
CREATE TABLE "public"."tb_user" (
"id" int4 DEFAULT nextval('tb_user_seq'::regclass) NOT NULL,
"user_name" varchar(255) COLLATE "default" NOT NULL,
"user_password" varchar(255) COLLATE "default" NOT NULL,
"is_active" bool DEFAULT true
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO "public"."tb_user" VALUES ('1', 'alper', '1', 't');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."tb_user_seq" OWNED BY "tb_user"."id";

-- ----------------------------
-- Primary Key structure for table tb_user
-- ----------------------------
ALTER TABLE "public"."tb_user" ADD PRIMARY KEY ("id");
