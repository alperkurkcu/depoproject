package com.depoproject.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleUtils {

	public static String getMessage(Locale locale, String messageKey) {
		ResourceBundle bundle = ResourceBundle.getBundle(LocaleList.BASE_NAME,
				locale);
		return bundle.getString(messageKey);
	}

	public static String getMessageParam(Locale locale, String messageKey,
			Object[] parameters) {
		ResourceBundle messages = ResourceBundle.getBundle(
				LocaleList.BASE_NAME, locale);
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(locale);
		formatter.applyPattern(messages.getString(messageKey));
		String output = formatter.format(parameters);
		return output;
	}

	public static <T> String getSimpleClassName(Class<T> objectClass) {
		return objectClass.toString().substring(
				objectClass.toString().lastIndexOf('.') + 1);
	}

}
