package com.depoproject.utils;

import java.io.IOException;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FacesContextUtils {

	public static void addErrorMessage(String messageKey) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessage(locale, messageKey);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}

	public static void addErrorMessageWithParams(String messageKey,
			Object[] params) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessageParam(locale,
				messageKey, params);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}

	public static void addInfoMessage(String messageKey) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessage(locale, messageKey);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}

	public static void addInfoMessageWithParams(String messageKey,
			Object[] params) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessageParam(locale,
				messageKey, params);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}

	public static void addWarnMessage(String messageKey) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessage(locale, messageKey);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}

	public static void addWarnMessageWithParams(String messageKey,
			Object[] params) {
		Locale locale = FacesContextUtils.getLocale();
		String message = ResourceBundleUtils.getMessageParam(locale,
				messageKey, params);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}

	public static void addPlainErrorMessage(String message) {
		FacesContext.getCurrentInstance()
				.addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, message,
								message));
	}

	public static void addPlainInfoMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
	}

	public static void addPlainWarnMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, message, message));
	}

	public static Object getSessionBean(String beanName) {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get(beanName);
	}

	public static Object getRequestBean(String beanName) {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(beanName);
	}

	public static String getLocaleName() {
		String localeName = FacesContext.getCurrentInstance().getViewRoot()
				.getLocale().getDisplayLanguage();
		return localeName;
	}

	public static Locale getLocale() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}

	public static void closeSession() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		session.invalidate();
		HttpServletResponse response = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		response.setHeader("Cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
	}

	public static String getParameter(String paramName) {
		String param = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get(paramName);
		return param;
	}

	public static <E> Object getViewBean(String beanName, Class<E> classOfBean) {
		FacesContext context = FacesContext.getCurrentInstance();
		E bean = (E) context.getApplication().evaluateExpressionGet(context,
				"#{" + beanName + "}", classOfBean);
		return bean;
	}

	public static void redirect(String page) {

		try {
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().redirect(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void refreshPage() {
		FacesContext fc = FacesContext.getCurrentInstance();
		String refreshpage = fc.getViewRoot().getViewId();
		ViewHandler ViewH = fc.getApplication().getViewHandler();
		UIViewRoot UIV = ViewH.createView(fc, refreshpage);
		UIV.setViewId(refreshpage);
		fc.setViewRoot(UIV);
		fc.renderResponse();
	}

	public static void newSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSession(true);
		javax.servlet.http.HttpSession h = (javax.servlet.http.HttpSession) obj;
		if (!h.isNew()) {
			h.invalidate();
			obj = context.getExternalContext().getSession(true);
			h = (javax.servlet.http.HttpSession) obj;
		}
	}

}