package com.depoproject.utils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for config complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="config">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntryPoint">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Http">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Https">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Employee" type="{http://emekboru.com/schema/emek}Employee"/>
 *         &lt;element name="Department" type="{http://emekboru.com/schema/emek}Department"/>
 *         &lt;element name="Pipe" type="{http://emekboru.com/schema/emek}Pipe"/>
 *         &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
 *         &lt;element name="Attributes" type="{http://emekboru.com/schema/emek}Attribute" maxOccurs="unbounded"/>
 *         &lt;element name="UnitMeasure" type="{http://emekboru.com/schema/emek}UnitMeasure" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Order" type="{http://emekboru.com/schema/emek}Order"/>
 *       &lt;/sequence>
 *       &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="author" type="{http://www.w3.org/2001/XMLSchema}string" />
 *        &lt;element name="TestDocs" type="{http://emekboru.com/schema/emek}TestDocs"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Config")
@XmlType(name = "config", propOrder = { "entryPoint" })
public class Config {

	@XmlElement(name = "EntryPoint", required = true)
	protected Config.EntryPoint entryPoint;
	@XmlAttribute(name = "date", required = true)
	protected String date;
	@XmlAttribute(name = "version", required = true)
	protected String version;
	@XmlAttribute(name = "author", required = true)
	protected String author;

	/**
	 * Gets the value of the entryPoint property.
	 * 
	 * @return possible object is {@link Config.EntryPoint }
	 * 
	 */
	public Config.EntryPoint getEntryPoint() {
		return entryPoint;
	}

	/**
	 * Sets the value of the entryPoint property.
	 * 
	 * @param value
	 *            allowed object is {@link Config.EntryPoint }
	 * 
	 */
	public void setEntryPoint(Config.EntryPoint value) {
		this.entryPoint = value;
	}

	/**
	 * Gets the value of the date property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDate(String value) {
		this.date = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}

	/**
	 * Gets the value of the author property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the value of the author property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthor(String value) {
		this.author = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Http">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="Https">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "http", "https" })
	public static class EntryPoint {

		@XmlElement(name = "Http", required = true)
		protected Config.EntryPoint.Http http;
		@XmlElement(name = "Https", required = true)
		protected Config.EntryPoint.Https https;

		/**
		 * Gets the value of the http property.
		 * 
		 * @return possible object is {@link Config.EntryPoint.Http }
		 * 
		 */
		public Config.EntryPoint.Http getHttp() {
			return http;
		}

		/**
		 * Sets the value of the http property.
		 * 
		 * @param value
		 *            allowed object is {@link Config.EntryPoint.Http }
		 * 
		 */
		public void setHttp(Config.EntryPoint.Http value) {
			this.http = value;
		}

		/**
		 * Gets the value of the https property.
		 * 
		 * @return possible object is {@link Config.EntryPoint.Https }
		 * 
		 */
		public Config.EntryPoint.Https getHttps() {
			return https;
		}

		/**
		 * Sets the value of the https property.
		 * 
		 * @param value
		 *            allowed object is {@link Config.EntryPoint.Https }
		 * 
		 */
		public void setHttps(Config.EntryPoint.Https value) {
			this.https = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Http {

			@XmlAttribute(name = "status")
			protected String status;
			@XmlAttribute(name = "value")
			protected String value;

			/**
			 * Gets the value of the status property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getStatus() {
				return status;
			}

			/**
			 * Sets the value of the status property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setStatus(String value) {
				this.status = value;
			}

			/**
			 * Gets the value of the value property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue() {
				return value;
			}

			/**
			 * Sets the value of the value property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value) {
				this.value = value;
			}

		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Https {

			@XmlAttribute(name = "status")
			protected String status;
			@XmlAttribute(name = "value")
			protected String value;

			/**
			 * Gets the value of the status property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getStatus() {
				return status;
			}

			/**
			 * Sets the value of the status property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setStatus(String value) {
				this.status = value;
			}

			/**
			 * Gets the value of the value property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue() {
				return value;
			}

			/**
			 * Sets the value of the value property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value) {
				this.value = value;
			}

		}

	}

}