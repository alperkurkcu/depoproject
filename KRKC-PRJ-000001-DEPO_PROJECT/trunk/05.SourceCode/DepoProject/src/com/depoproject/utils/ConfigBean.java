package com.depoproject.utils;

import java.io.File;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.primefaces.push.annotation.Singleton;

@Singleton
public class ConfigBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Config config;
	private File configFile;
	private String entryPoint;
	private PageUrl pageUrl;

	public ConfigBean() {

		System.getProperty("java.class.path");
		// URL resourcePath = getClass().getResource("emek.xml");

		// configFile = new File(resourcePath.getPath());
		try {
			JAXBContext jaxb = JAXBContext.newInstance(Config.class);
			Unmarshaller unM = jaxb.createUnmarshaller();
			config = (Config) unM.unmarshal(configFile);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {

		boolean httpStatus = Boolean.parseBoolean(config.getEntryPoint()
				.getHttp().getStatus());
		boolean httpsStatus = Boolean.parseBoolean(config.getEntryPoint()
				.getHttps().getStatus());

		if (httpStatus)
			entryPoint = config.getEntryPoint().getHttp().getValue();
		else if (httpsStatus)
			entryPoint = config.getEntryPoint().getHttps().getValue();

		else {

			String msg = "Error parsing xml configuration file!"
					+ "At least one entry point should be active!";

			throw new IllegalArgumentException(msg);
		}

		pageUrl = new PageUrl(entryPoint);
	}

	public Config getConfig() {

		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public PageUrl getPageUrl() {

		return pageUrl;
	}

	public String getEntryPoint() {

		return entryPoint;
	}

	public static class PageUrl {

		public String LOGIN_PAGE;
		public String STOK_MAIN_PAGE;

		public PageUrl(String baseUrl) {

			// init(baseUrl);
			init("/DepoProject/");
		}

		public void init(String baseUrl) {
			LOGIN_PAGE = baseUrl + "loginpage.jsf?faces-redirect=true";
			STOK_MAIN_PAGE = baseUrl + "stokpage.jsf?faces-redirect=true";

		}
	}
}