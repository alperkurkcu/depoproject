package com.depoproject.jpamanager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.depoproject.jpa.TbUser;
import com.depoproject.jpamanager.commons.CommonQueries;
import com.depoproject.jpamanager.commons.Factory;

public class TbUserManager extends CommonQueries<TbUser> {

	@SuppressWarnings("unchecked")
	public List<TbUser> userLogin(String username, String password) {

		List<TbUser> userList = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		userList = manager.createNamedQuery("TbUser.findUser")
				.setParameter("username", username)
				.setParameter("password", password).getResultList();
		return userList;
	}

}
