package com.depoproject.jpamanager.commons;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Factory {

	private static EntityManagerFactory emf;
	private static EntityManager em = null;
	static {
		try {
			emf = Persistence.createEntityManagerFactory("DepoProject");
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void closeFactory() {
		emf.close();
	}

	public static EntityManagerFactory getInstance() {
		return emf;
	}

	public static synchronized EntityManager getEntityManager() {
		if (em == null) {
			if (emf == null)
				emf = Persistence.createEntityManagerFactory("DepoProject");
			em = emf.createEntityManager();
		}
		return em;
	}

	public static EntityTransaction getTransaction() {
		if (em != null)
			return em.getTransaction();
		else {
			return getEntityManager().getTransaction();
		}
	}

	public static void beginTransaction() {
		EntityTransaction transaction = getTransaction();
		if (transaction != null)
			transaction.begin();
	}

	public static void commitTransaction() {
		EntityTransaction transaction = getTransaction();
		if (transaction != null)
			transaction.commit();

	}

}
