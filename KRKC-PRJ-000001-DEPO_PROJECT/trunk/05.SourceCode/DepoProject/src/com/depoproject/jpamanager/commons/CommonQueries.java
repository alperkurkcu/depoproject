package com.depoproject.jpamanager.commons;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class CommonQueries<T> {

	public boolean enterNew(T object) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();

		tx.begin();
		manager.persist(object);
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return true;

	}

}
