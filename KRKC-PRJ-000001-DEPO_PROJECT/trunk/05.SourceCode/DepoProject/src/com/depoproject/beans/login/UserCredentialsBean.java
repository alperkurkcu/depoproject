package com.depoproject.beans.login;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import com.depoproject.jpa.TbUser;
import com.depoproject.jpamanager.TbUserManager;
import com.depoproject.utils.ConfigBean;
import com.depoproject.utils.FacesContextUtils;

@ManagedBean(name = "userCredentialsBean")
@SessionScoped
public class UserCredentialsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean configBean;

	private TbUser user;

	private String username;
	private String password;
	private String userFullName;
	private String userPicturePath;
	private String dateFormat;
	private String dateFormatWithTime;

	public UserCredentialsBean() {

		dateFormat = new String("dd/MM/yyyy");
		dateFormatWithTime = new String("dd/MM/yyyy HH:mm");
		username = new String();
		password = new String();
		user = new TbUser();
		userPicturePath = null;
	}

	public void loginUser(ActionEvent e) {
		TbUserManager userManager = new TbUserManager();
		List<TbUser> systemUser = userManager.userLogin(username, password);
		if (systemUser.size() == 0) {

			FacesContextUtils.addErrorMessage("LoginError");
			return;
		}
		user = systemUser.get(0);
		if (!user.isActive()) {

			FacesContextUtils.addErrorMessage("UserDisabledError");
			return;
		}

		FacesContextUtils.redirect("stokpage.jsf");
		// FacesContextUtils.redirect(configBean.getPageUrl().STOK_MAIN_PAGE);

		userFullName = user.getUserName();
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		request.setAttribute("user", userFullName);

		userPicturePath = user.getUserName() + "_" + user.getId();
	}

	// setter getters

	public TbUser getUser() {
		return user;
	}

	public void setUser(TbUser user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

}
