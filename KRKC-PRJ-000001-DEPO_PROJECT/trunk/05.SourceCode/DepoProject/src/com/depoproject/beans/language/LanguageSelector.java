package com.depoproject.beans.language;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "languageSelector")
@SessionScoped
public class LanguageSelector implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -361252668569032857L;
	private String language = "";
	private String locale = "";
	private String basename = "";

	public LanguageSelector() {
		locale = "tr";
		basename = "messages";
		language = "Turkish";
	}

	public String getLanguage() {
		return language;
	}

	public void setEnglishLanguage(ActionEvent a) {
		locale = "en";
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(new Locale("en"));
	}

	public void setTurkishLanguage(ActionEvent a) {
		locale = "tr";
		FacesContext.getCurrentInstance().getViewRoot()
				.setLocale(new Locale("tr"));
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getBasename() {
		return basename;
	}

	public void setBasename(String basename) {
		this.basename = basename;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}